# RetroPie Arcade Machine
Using a Raspberry Pi equipped with [RetroPie](https://retropie.org.uk/), my brothers and I created an old school arcade machine similar to others you may have seen.
The Pi holds emulators that can play ROMS of older games that used to be on these arcade machines.

## The Cabinet
Made from plywood, the cabinet is shaped like a standing arcade machine.

## Additional Work to RetroPie
- RetroPie needs specific emulators and is not normally equipped with the emulators needed to play all the old arcade games that we had wanted. Doing some research, I eventually went with the MAME 2003+ emulator as the specs used to emulate games fit with my Raspberry Pi 3B+.

- Adding actual arcade buttons is a huge deal when making an arcade machine! Buttons were purchases, plugged into a controlling board and mapped to the Pi.

- Wi-Fi connectability was originally scripted out (with the help of some online guides!). In more recent updates, RetroPie has their own server hosting that can be reached on your own network with a setting, which is now what I use as the functionality is much more robust. Adding ROMs from my Desktop is highly preferable over using a USB!

- Two extension cords, one with an on/off switch, and an outlet extender are used to mimic the power on and off switch of an arcade machine. A slot is cut to allow the on/off switch to be accessible externally, so both the Pi and the monitor are turned on and off by the switch.

## Components
- Raspberry Pi 3B+
- [2 Player LED Arcade Buttons](https://www.amazon.com/gp/product/B07JF34XPB/ref=ppx_yo_dt_b_search_asin_title?ie=UTF8&psc=1)
- Older 21' Monitor with HDMI input
- 3' Extension Cord
- [Short Extension Cord with On/Off Switch](https://www.amazon.com/gp/product/B08P7SVP5T/ref=ppx_yo_dt_b_asin_title_o07_s01?ie=UTF8&psc=1)
- Outlet Extender (2 outlets)

**All work has been done within RetroPie and physically. No cool code to show here!**
